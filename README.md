# Project description

Detect and assign correct rental type variable. Run `get_rental_type.py` and it will store the dfs of unique identifiers (listing and transaction ID) and a binary 1 if classified as single room rental and 0 otherwise in module scope.

Args:
1. {"transaction_year":2021,"buffer":100}
2. {"floor_area_in_sqm":30 , "pps_percentile":0.1 , "listing_price":1800}
### Listing - 

PK: dw_listing_id

### Transactions (VB) - 

PK: dw_transaction_id

## Logic

### For HDB - 

1. https://blog.carousell.com/property/hdb-room-rental-cost/
2. Adjust rental cost by regional HDB rental indices change to 2021 (transaction_year)
3. If listing price <= average adjusted rental cost + 100 (buffer) , then single room. Else not.

### For Condo/Landed - 

Let <br>
A - Floor area sqm direct filter  - <= 30sqm. <br>
B - Price per sqm filter - <= 0.10 or 10th percentile <br>
C - Price filter - <= $1.8k 

Then <br>
{S ∈ {0,1} | S = (B ∩ C) ∪ A}

## Links and data

1. https://blog.carousell.com/property/hdb-room-rental-cost/
