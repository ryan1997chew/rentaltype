
'''
This script writes the 2019 Carousell HDB average regional/zonal single room rental rates into DB.
We then adjust the rates to the current year using the rental index. This is done all via SQL scripts.
Then, it returns dfs of the listing/transactions (dw_listing_id and dw_transaction_id as the unique identifiers) and a binary
variable representing the outcome: 1 for single room rental, 0 for not.
'''


import pandas as pd
import os
import config as config
from rea_python.main.database import DBCopyMode, RedshiftHook
from rea_python.constants import OutputFormat


sql_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sql')

# initialize redshift hook
src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                        via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                        via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

# set up connection
src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)
# load queries from sql
src_hook.load_queries_from_folders([sql_path])


#b - benchmark, h - hdb
bh = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath('rental_type.ipynb'))),'input'),'hdb_avg_single_room_rental.csv')
bh = pd.read_csv(bh)

#write the 2019 Carousell average HDB single room rental rates into the DB.
src_hook.copy_from_df(df=bh, target_table='data_science.sg_project_hdb_rental_rates', mode=DBCopyMode.DROP)

def query(query_str, template_parameters=None):
    return src_hook.execute_loaded_query(query_name=query_str, output_format=OutputFormat.pandas, template_parameters=template_parameters)

def save_adjusted_rental_rates(template_parameters):
    '''
    Execute adjusted_rental_rates query, store it into a df, and then put this df into the table. This is only for HDB.
    transaction_year : int. A year, aka 2021. You want the latest year you are in.
    buffer: int. Amount in dollars you add to the average single room rental rate, to form your upper bound. So anything within this bound is considered a single room rental.
    '''
    arr = query('adjusted_rental_rates', template_parameters)
    src_hook.copy_from_df(df=arr, target_table='data_science.sg_project_adjusted_rental_rates', mode=DBCopyMode.DROP)


save_adjusted_rental_rates({"transaction_year":2021,"buffer":100})
listings_hdb = query('listings_hdb')
listings_condo = query('listings_condo', {"floor_area_in_sqm":30 , "pps_percentile":0.1 , "listing_price":1800})
listings_landed = query('listings_landed', {"floor_area_in_sqm":30 , "pps_percentile":0.1 , "listing_price":1800})
transaction_hdb = query('transaction_hdb')

pairing = ['dw_listing_id','property_type','single_room_rental']
overall = [listings_hdb[pairing], listings_condo[pairing], listings_landed[pairing]]
result = pd.concat(overall)

#drop arr
src_hook.execute_raw_query(query = 'DROP TABLE IF EXISTS data_science.sg_project_adjusted_rental_rates')

print("Finished")