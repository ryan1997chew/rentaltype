from ..common import *
from rea_python.main.aws import get_secret

REDSHIFT_HOOK_BRIDGE_BUCKET = "dev-misc-usage"
DB_URI = get_secret("legacy/data_team/db_conn_uri")
DATA_WAREHOUSE_URI = get_secret("prod/redshift/pipeline/db_conn_uri")