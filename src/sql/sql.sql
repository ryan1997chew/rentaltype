-- name: listings_hdb
with hdb as (
select
case
	when vpe.zone = 'bukit timah' then 'central'
	when vpe.zone = 'downtown core' then 'central'
	when vpe.zone = 'novena' then 'central'
	when vpe.zone = 'outram' then 'central'
	when vpe.zone = 'rochor' then 'central'
	when vpe.zone = 'tanglin' then 'central'
	when vpe.zone = 'changi' then 'pasir ris'
else vpe.zone end as zone,
 vpe.postal_code, vle.dw_listing_id, vle.dw_property_id,vle.listing_price
from
	datamart.view_listing_expand vle
	inner join datamart.view_property_expand vpe on vpe.dw_property_id = vle.dw_property_id
where
	vle.country = 'sg' and
	vle.listing_type = 'rent' and
	(vle.property_type = 'HDB' or vpe.property_type_group = 'HDB') and
	vle.listing_price is not null
order by
	listing_price desc)
select
	hdb.*,
	round(arr.listing_price_threshold,1) as listing_price_threshold,
	case when arr.listing_price_threshold >= listing_price then 1 else  0 end as single_room_rental,
    'hdb' as property_type
from
	hdb
	inner join data_science.sg_project_adjusted_rental_rates arr on hdb.zone = arr.zone


-- name: listings_condo
with condo as (
select
 vle.project_name, vle.dw_listing_id, vle.dw_property_id,
 vle.listing_price,
 coalesce (vle.floor_area_sqm, vle.built_up_area_sqm) as floor_area_in_sqm,
 vle.listing_price/floor_area_in_sqm as price_per_sqm,
 vle.unit_layout,
 percent_rank() over (order by price_per_sqm) as pps_percentile
from
	datamart.view_listing_expand vle
	inner join datamart.view_property_expand vpe on vpe.dw_property_id = vle.dw_property_id
where
	vle.country = 'sg' and
	vle.listing_type = 'rent' and
	(vle.property_type = 'Condo' or vpe.property_type_group = 'Condo')and
	vle.listing_price is not null
order by
	price_per_sqm asc)
select
	*,
	case when floor_area_in_sqm < {{ floor_area_in_sqm }} then 1 else 0 end as floor_area_criteria,
	case when pps_percentile <= {{ pps_percentile }} then 1 else 0 end as price_per_sqm_criteria,
	case when listing_price <= {{ listing_price }} then 1 else 0 end as listing_price_criteria,
	case when (price_per_sqm_criteria and listing_price_criteria = 1) or floor_area_criteria = 1 then 1 else 0 end as single_room_rental,
    'condo' as property_type
from
	condo


	-- name: listings_landed
with landed as (
select
 vle.project_name, vle.dw_listing_id, vle.dw_property_id,
 vle.listing_price,
 coalesce (vle.floor_area_sqm, vle.built_up_area_sqm) as floor_area_in_sqm,
 vle.listing_price/floor_area_in_sqm as price_per_sqm,
 vle.unit_layout,
 percent_rank() over (order by price_per_sqm) as pps_percentile
from
	datamart.view_listing_expand vle
	inner join datamart.view_property_expand vpe on vpe.dw_property_id = vle.dw_property_id
where
	vle.country = 'sg' and
	vle.listing_type = 'rent' and
	(vle.property_type = 'Landed' or vpe.property_type_group  = 'Landed') and
	vle.listing_price is not null
order by
	price_per_sqm asc)
select
	*,
	case when floor_area_in_sqm < {{ floor_area_in_sqm }} then 1 else 0 end as floor_area_criteria,
	case when pps_percentile <= {{ pps_percentile }} then 1 else 0 end as price_per_sqm_criteria,
	case when listing_price <= {{ listing_price }} then 1 else 0 end as listing_price_criteria,
	case when (price_per_sqm_criteria and listing_price_criteria = 1) or floor_area_criteria = 1 then 1 else 0 end as single_room_rental,
    'landed' as property_type
from
	landed



-- name: transaction_hdb

with hdb as(
select
	vb.dw_transaction_id,
	vb.dw_property_id,
	replace(replace(vb.region,'region','') ,'-',' ') as region,
	vb.transaction_amount
from
	data_science.valuation_base vb
where
	country = 'sg' and
	transaction_type = 'rent' and
	property_type_group = 'HDB'
order by
	transaction_amount desc)
select
	hdb.*,
	arr.listing_price_threshold,
	case when arr.listing_price_threshold >= hdb.transaction_amount then 1 else  0 end as single_room_rental
from
	hdb
	left join data_science.sg_project_adjusted_rental_rates arr on hdb.region = arr.region
;



-- name: adjusted_rental_rates
with latest_index as (
with yearly_index as (
select
	left(transaction_month_index,4) as transaction_year,
	avg(hi) as index,
	 replace(index_area_name, '-',' ') as region
from
	data_science.sg_index_rent sir
where
	index_area_type = 'region' and
	property_group = 'hdb'
group by
 	index_area_name, transaction_year
 having
 	index_area_name = 'central' or index_area_name = 'west' or index_area_name = 'east' or index_area_name ='north-east' or index_area_name = 'north' and
	 transaction_year = {{ transaction_year}}
order by
	transaction_year)
select
	*
from
	yearly_index
where
	transaction_year = {{ transaction_year }})
select
	rr.zone,
	rr.region,
	round(rr.listing_price * latest_index.index,2) + {{ buffer }} as listing_price_threshold
from
	data_science.sg_project_hdb_rental_rates rr
	inner join latest_index on rr.region = latest_index.region
;



